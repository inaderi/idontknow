from django.core.paginator import Paginator
from django.http import HttpResponse
from django.shortcuts import render


# Create your views here.
from django.template import loader
from django.views import generic
from django.views.generic import ListView

from tasks.models import Tasks


def hello(request):
    return HttpResponse("Hello, world.")


class TaskListView(generic.ListView):
    model = Tasks
    template_name = 'tasks/homepage.html'  # Default: <app_label>/<model_name>_list.html
    context_object_name = 'result'  # Default: object_list
    paginate_by = 2
    queryset = Tasks.objects.all()

def first_page(request):
    result = Tasks.objects.all()
    paginator = Paginator(result, 25)

    page_number = request.GET.get('page')
    if request.GET.get('page'):
        page_obj = paginator.get_page(page_number)
    else:
        page_obj =0
    template = loader.get_template('tasks/homepage.html')
    context = {
        'result': result,
    }
    return HttpResponse(template.render(request, context))


