from django.conf.urls import url
from django.contrib import admin
from django.urls import path

from tasks import views
from tasks.views import TaskListView

urlpatterns = [
    url(r'^$', TaskListView.as_view(), name='index'),
    # path('', views.hello,name='hello'),
]
