from django.db import models


from django.contrib.auth.models import User

class Peymankar(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

class Tasks(models.Model):
    subject = models.CharField(max_length=50)
    estimated_days = models.IntegerField(default=23)
    value = models.IntegerField(default=10)
    deadline = models.TimeField(auto_now_add=True)
    owner = models.ForeignKey(User,on_delete=models.CASCADE)
    details = models.TextField(blank=True)

